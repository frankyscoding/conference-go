import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + f"{state}",
        "per_page": 1,
    }
    r = requests.get(url, params=params, headers=headers)
    content = json.loads(r.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    r = requests.get(url, params=params)
    content = json.loads(r.content)

    lat = content[0]["lat"]
    lon = content[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {"lat": lat, "lon": lon, "appid": OPEN_WEATHER_API_KEY}
    r = requests.get(url, params=params)
    content = json.loads(r.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": (content["main"]["temp"] - 273.15) * (9 / 5) + 32,
        }
    except (KeyError, IndexError):
        return None
